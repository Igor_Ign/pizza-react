import { useEffect, useState } from 'react'
import useWindowSize from './useWindowSize'

const useOffset = () => {
  const [offsetX, setOffsetX] = useState(0)
  const [currentSlide, setCurrentSlide] = useState(0)

  const { width } = useWindowSize()

  useEffect(() => {
    if (width && width >= 769) {
      setOffsetX(0)
      setCurrentSlide(0)
    }
  }, [width])

  const onChangeHandler = (index: number) => {
    switch (index) {
      case 0: {
        setOffsetX(0)
        setCurrentSlide(0)
        break
      }
      case 1: {
        setOffsetX(320)
        setCurrentSlide(1)
        break
      }
      case 2: {
        setOffsetX(642)
        setCurrentSlide(2)
        break
      }
      default: {
        break
      }
    }
  }

  return {
    offsetX,
    currentSlide,
    onChangeHandler,
  }
}

export default useOffset
