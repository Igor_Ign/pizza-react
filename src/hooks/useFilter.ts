import { useCallback, useMemo, useState } from 'react'

import TPizzaTypes from '../models/pizza-types.model'

import { IPizza } from '../models/pizza.model'

const useFilter = (data: IPizza[]) => {
  const [currentFilter, setCurrentFilter] = useState<TPizzaTypes>(TPizzaTypes.ALL)

  const onChangeFilterHandler = useCallback(
    (newFilter: TPizzaTypes) => setCurrentFilter(newFilter),
    [],
  )

  const filteredData = useMemo(() => {
    if (currentFilter === TPizzaTypes.ALL) {
      return data
    }

    return data.filter((f) => f.types.some((t) => t === currentFilter))
  }, [currentFilter])

  return {
    filteredData,
    onChangeFilterHandler,
    currentFilter,
  }
}

export default useFilter
