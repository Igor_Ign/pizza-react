import { useCallback, useState } from 'react'

type ReturnedTypes = [boolean, () => void]

const useToggle = (): ReturnedTypes => {
  const [isOpen, setIsOpen] = useState(false)
  const openHandler = useCallback(() => setIsOpen(!isOpen), [isOpen])

  return [isOpen, openHandler]
}

export default useToggle
