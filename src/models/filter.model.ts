import TPizzaTypes from './pizza-types.model'

export interface IFilter {
  id: number
  type: TPizzaTypes
}
