enum TPizzaTypes {
  ALL = 'Все',
  PEPPER = 'Острые',
  MEAT = 'Мясные',
  CHEESE = 'Сырные',
  VEGAN = 'Веганские',
}

export default TPizzaTypes
