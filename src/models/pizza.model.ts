import TPizzaTypes from './pizza-types.model'

export interface IPizza {
  id: number
  imgUrl: string
  title: string
  recipe: string
  sizes: TSize[]
  currentSize: TSize
  cost: number
  types: TPizzaTypes[]
}

export type TSize = 20 | 30 | 40
