import React from 'react'

import Layout from './components/Layout'
import MainSection from './components/Sections/MainSection'
import PromotionsSection from './components/Sections/ PromotionsSection'
import PizzaSection from './components/Sections/PizzaSection'
import DeliverySection from './components/Sections/DeliverySection'
import AboutSection from './components/Sections/AboutSection'
import GallerySection from './components/Sections/GallerySection'

function App() {
  return (
    <Layout>
      <MainSection />
      <PromotionsSection />
      <PizzaSection />
      <DeliverySection />
      <AboutSection />
      <GallerySection />
    </Layout>
  )
}

export default App
