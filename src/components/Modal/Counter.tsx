import React from 'react'

import minusIcon from './../../assets/img/svg/minus.svg'
import plusIcon from './../../assets/img/svg/plus.svg'

const Counter = () => {
  return (
    <div className='modal__counter'>
      <button className='modal__counter-minus'>
        <img src={minusIcon} alt='minus' />
      </button>
      <input className='modal__counter-field' type='text' />
      <button className='modal__counter-plus'>
        <img src={plusIcon} alt='plus' />
      </button>
    </div>
  )
}

export default Counter
