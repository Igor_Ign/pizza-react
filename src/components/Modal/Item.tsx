import React from 'react'
import Counter from './Counter'

import PepperIcon from '../UI/SVGIcons/PepperIcon'

import pizzaImg from '../../assets/img/pizza/marg.png'
import closeIcon from '../../assets/img/svg/close.svg'

const Item = () => {
  return (
    <div className='modal__item'>
      <div className='modal__pizza'>
        <div className='modal__pizza-type'>
          <PepperIcon className='modal__pizza-icon' />
          <img className='modal__pizza-img' src={pizzaImg} alt='pizza' />
        </div>
        <div className='modal__pizza-info'>
          <p className='modal__pizza-name'>BBQ</p>
          <p className='modal__pizza-size'>30sm</p>
        </div>
      </div>
      <Counter />
      <div className='modal__cost'>1235 rub</div>
      <button className='modal__remove'>
        <img className='modal__remove-img' src={closeIcon} alt='close' />
      </button>
    </div>
  )
}

export default Item
