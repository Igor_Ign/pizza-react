import React, { FC, useState } from 'react'

import Item from './Item'
import Input from '../UI/Input'
import RadioButton from '../UI/RadioButton'
import Button from '../UI/Button'

import closeIcon from '../../assets/img/svg/close.svg'

import './modalStyles.sass'

interface ModalProps {
  isOpen: boolean
  modalToggle: () => void
}

const Modal: FC<ModalProps> = ({ isOpen, modalToggle }) => {
  const [checked, setChecked] = useState('deliver')

  const onChangeHandler = (ev: React.ChangeEvent<HTMLInputElement>) => setChecked(ev.target.name)

  return (
    <>
      <div className={`backdrop ${isOpen ? 'backdrop_show' : ''}`} />
      <div className={`modal ${isOpen ? 'open' : 'close'}`}>
        <div className='modal__header'>
          <p className='modal__title'>Ваш заказ</p>
          <button className='modal__close' onClick={modalToggle}>
            <img className='modal__close-img' src={closeIcon} alt='close' />
          </button>
        </div>

        <Item />

        <div className='modal__total'>
          Сумма заказа: <span>1234 руб</span>
        </div>

        <form className='modal__form'>
          <p className='modal__form-title'>Контакты</p>
          <div className='modal__form-wrapper'>
            <div className='modal__form-input-wrapper'>
              <Input label='name' placeholder='Ваше имя' />
              <Input label='phone' placeholder='Телефон' />
            </div>
            <Input label='address' placeholder='Адрес доставки' />
          </div>
          <p className='modal__form-title'>Способ оплаты</p>
          <div className='modal__form-radio-wrapper'>
            <RadioButton
              name='deliver'
              checked={checked === 'deliver'}
              title='Оплата наличной или картой курьеру'
              onChange={onChangeHandler}
            />
            <RadioButton
              name='site'
              checked={checked === 'site'}
              title='Оплата картой онлайн на сайт'
              onChange={onChangeHandler}
            />
          </div>
          <Button className='button modal__form-submit'>Оформить заказ</Button>

          <p className='modal__form-politic'>
            Нажимая кнопку «Оформить заказ» вы соглашаетесь с политикой конфиденциальности
          </p>
        </form>
      </div>
    </>
  )
}

export default Modal
