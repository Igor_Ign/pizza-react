import React, { FC } from 'react'

import Menu from '../Menu'
import MobileMenu from '../MobileMenu'
import Info from '../Info'

import logo from '../../assets/img/svg/logo.svg'
import cartIcon from '../../assets/img/svg/cart.svg'
import phoneIcon from '../../assets/img/svg/phone.svg'
import menuIcon from '../../assets/img/svg/menu.svg'

import './headerStyles.sass'

interface HeaderProps {
  isOpenMenu: boolean
  menuToggle: () => void
  modalToggle: () => void
}

const Header: FC<HeaderProps> = ({ isOpenMenu, modalToggle, menuToggle }) => {
  return (
    <header className='header'>
      <div className='container'>
        <div className='header__inner'>
          <a href='/' className='header__logo-container'>
            <img className='header__logo' src={logo} alt='logo' />
          </a>

          <Menu />
          <MobileMenu isOpen={isOpenMenu} onChange={menuToggle} />
          <div className='header__info-container'>
            <Info
              icon={phoneIcon}
              alt='phone'
              title='+7 (918) 432-65-87'
              subtitle='Ежедневно с 9:00 до 23:00'
              isLink
              href='tel:+79184326587'
            />

            <Info
              icon={cartIcon}
              alt='cart'
              title='Ваш заказ'
              subtitle='Итальянская и ещё 2 пиццы'
              counter={3}
              onClick={modalToggle}
            />

            <div className='header__lang'>En</div>

            <button className='header__hamburger'>
              <img src={menuIcon} alt='menu' onClick={menuToggle} />
            </button>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header
