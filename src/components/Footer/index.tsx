import React from 'react'

import Logo from '../../assets/img/svg/logo.svg'

import './footerStyles.sass'

const Footer = () => {
  return (
    <footer className='footer'>
      <div className='container'>
        <div className='footer__inner'>
          <img src={Logo} alt='logo' className='footer__logo' />
          <div className='footer__info'>
            <a href='tel:+79184236587' className='footer__phone'>
              +7 (918) 432-65-87
            </a>
            <p className='footer__time'>Ежедневно с 9:00 до 23:00</p>
          </div>
          <a href='#' className='footer__politic'>
            Политика конфиденциальности
          </a>
        </div>
      </div>
    </footer>
  )
}

export default Footer
