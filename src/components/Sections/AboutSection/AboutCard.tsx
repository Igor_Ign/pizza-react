import React, { FC } from 'react'

interface AboutCardProps {
  img: string
  alt: string
  title: string
  description: string
  reverse?: boolean
}

const AboutCard: FC<AboutCardProps> = ({ title, description, img, alt, reverse = false }) => {
  const isReverseClass =
    reverse && window.screen.width > 640 ? 'about-section__card-img_reverse' : ''

  return (
    <div className='about-section__card'>
      <img src={img} alt={alt} className={`about-section__card-img ${isReverseClass}`} />
      <div className='about-section__card-wrapper'>
        <h3 className='about-section__card-title'>{title}</h3>
        <p className='about-section__card-description'>{description}</p>
      </div>
    </div>
  )
}

export default AboutCard
