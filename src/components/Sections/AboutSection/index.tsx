import React from 'react'

import AboutCard from './AboutCard'

import aboutOneImg from '../../../assets/img/about/about01.jpg'
import aboutTwoImg from '../../../assets/img/about/about02.jpg'
import aboutThreeImg from '../../../assets/img/about/about03.jpg'

import './aboutSectionStyles.sass'

const AboutSection = () => {
  return (
    <section className='about-section'>
      <div className='container'>
        <div className='about-section__inner'>
          <AboutCard
            title='Изготавливаем пиццу по своим рецептам в лучших традициях'
            description='Наша пицца получается сочной, вкусной и главное хрустящей с нежной и аппетитной начинкой, готовим по своим итальянским рецептам'
            img={aboutOneImg}
            alt=''
          />
          <AboutCard
            title='Используем только свежие ингридиенты'
            description='Ежедневно заготавливаем продукты и овощи для наших пицц, соблюдаем все сроки хранения'
            img={aboutTwoImg}
            alt=''
            reverse
          />
          <AboutCard
            title='Доставка в течение 60 минут или заказ за нас счёт'
            description='Все наши курьеры – фанаты серии Need for Speed и призеры гонок World Rally Championship и World Superbike во всех категориях'
            img={aboutThreeImg}
            alt=''
          />
        </div>
      </div>
    </section>
  )
}

export default AboutSection
