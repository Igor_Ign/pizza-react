import React from 'react'
import Button from '../../UI/Button'

import img from '../../../assets/img/pizza-bg.png'

import './mainSectionStyles.sass'

const MainSection = () => {
  return (
    <section className='main-section'>
      <img className='main-section__bg' src={img} alt='pizza-bg' />
      <div className='container'>
        <div className='main-section__inner'>
          <h1 className='main-section__title'>Пицца на заказ</h1>
          <p className='main-section__description'>
            Бесплатная и быстрая доставка за чаc в любое удобное для вас время
          </p>
          <Button className='button main-section__btn'>Выбрать пиццу</Button>
        </div>
      </div>
    </section>
  )
}

export default MainSection
