import React, { FC } from 'react'

interface PromoCardProps {
  src: string
  alt: string
  title: string
  description: string
}

const PromoCard: FC<PromoCardProps> = ({ description, title, src, alt }) => {
  return (
    <div className='promo-section__card'>
      <img className='promo-section__img' src={src} alt={alt} />
      <h4 className='promo-section__title'>{title}</h4>
      <p className='promo-section__description'>{description}</p>
    </div>
  )
}

export default PromoCard
