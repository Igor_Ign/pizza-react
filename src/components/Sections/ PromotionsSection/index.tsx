import React from 'react'

import PromoCard from './PromoCard'

import useOffset from '../../../hooks/useOffset'

import promo1 from '../../../assets/img/promo/promo1.jpg'
import promo2 from '../../../assets/img/promo/promo2.jpg'
import promo3 from '../../../assets/img/promo/promo3.jpg'

import './promotionsSectionStyles.sass'

const PromotionsSection = () => {
  const { offsetX, currentSlide, onChangeHandler } = useOffset()

  return (
    <section className='promo-section'>
      <div className='container'>
        <div className='promo-section__list'>
          <div className='promo-section__list-inner' style={{ left: `-${offsetX}px` }}>
            <PromoCard
              src={promo1}
              alt='promo1'
              title='Закажи 2 пиццы – 3-я в подарок'
              description='При заказе 2-х больших пицц – средняя пицца в подарок'
            />
            <PromoCard
              src={promo2}
              alt='promo2'
              title='Напиток в подарок'
              description='Скидка на заказ от 3 000 рублей + напиток в подарок'
            />
            <PromoCard
              src={promo3}
              alt='promo3'
              title='25% при первом заказе'
              description='Скидка новым клиентам!'
            />
          </div>
        </div>
        <div className='promo-section__list-dots'>
          {[1, 2, 3].map((_, index) => (
            <span
              key={index}
              onClick={() => onChangeHandler(index)}
              className={`promo-section__list-dot ${
                index === currentSlide ? 'promo-section__list-dot_active' : ''
              }`}
            />
          ))}
        </div>
      </div>
    </section>
  )
}

export default PromotionsSection
