import React, { FC, useState } from 'react'

import { TSize } from '../../../models/pizza.model'

import changeSize from '../../../helpers/changeSize'

interface PizzaSizeProps {
  sizes: TSize[]
  currentSize: TSize
}

const PizzaSize: FC<PizzaSizeProps> = ({ currentSize, sizes }) => {
  const [size, setSize] = useState(currentSize)

  const onChangeSizeHandler = (newSize: TSize) => setSize(newSize)

  return (
    <div className='pizza-section__size'>
      <p className='pizza-section__size-title'>Размер, см:</p>
      <div className='pizza-section__size-wrapper'>
        <div className='pizza-section__size-pointer' style={{ left: `${changeSize(size)}%` }} />
        {sizes?.map((s) => (
          <div
            key={s}
            onClick={() => onChangeSizeHandler(s)}
            className={`pizza-section__size-diameter `}
          >
            {s}
          </div>
        ))}
      </div>
    </div>
  )
}

export default PizzaSize
