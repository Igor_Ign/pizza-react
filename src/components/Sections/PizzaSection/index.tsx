import React from 'react'

import mockPizza from './mockPizza'
import FILTERS from '../../../constance/filters'
import useFilter from '../../../hooks/useFilter'

import PizzaCard from './PizzaCard'

import './pizzaSectionStyles.sass'

const PizzaSection = () => {
  const { filteredData, onChangeFilterHandler, currentFilter } = useFilter(mockPizza)

  return (
    <section className='pizza-section'>
      <div className='container'>
        <h2 className='pizza-section__title'>Выберите пиццу</h2>

        <ul className='pizza-section__list'>
          {FILTERS.map((filter) => (
            <li
              key={filter.id}
              className='pizza-section__item'
              onClick={() => onChangeFilterHandler(filter.type)}
            >
              <span
                className={`pizza-section__item-text ${
                  currentFilter === filter.type ? 'active' : ''
                }`}
              >
                {filter.type}
              </span>
              <img
                className='pizza-section__item-icon active'
                src={require(`../../../assets/img/svg/filter/${filter.type}.svg`)}
                alt={filter.type}
              />
            </li>
          ))}
        </ul>
        <div className='pizza-section__inner'>
          {filteredData && filteredData.map((pizza) => <PizzaCard key={pizza.id} {...pizza} />)}
        </div>
      </div>
    </section>
  )
}

export default PizzaSection
