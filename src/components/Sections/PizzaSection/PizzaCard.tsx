import React, { FC } from 'react'

import { IPizza } from '../../../models/pizza.model'

import PizzaSize from './PizzaSize'
import Button from '../../UI/Button'

const PizzaCard: FC<IPizza> = ({ types, cost, title, recipe, sizes, imgUrl, currentSize }) => {
  return (
    <div className='pizza-section__card'>
      <div className='pizza-section__card-icon-wrapper'>
        {types.map((type) => (
          <img
            key={type}
            className='pizza-section__card-icon'
            src={require(`../../../assets/img/svg/filter/${type}.svg`)}
            alt={type}
          />
        ))}
      </div>

      <div className='pizza-section__card-wrapper'>
        <div className='pizza-section__card-container'>
          <img className='pizza-section__card-img' src={imgUrl} alt='pizza' />
        </div>
        <div className='pizza-section__card-border' />
        <div className='pizza-section__card-border pizza-section__card-border_md' />
        <div className='pizza-section__card-border pizza-section__card-border_lg' />
      </div>
      <div>
        <h4 className='pizza-section__card-title'>{title}</h4>
        <p className='pizza-section__card-recipe'>{recipe}</p>

        <PizzaSize sizes={sizes} currentSize={currentSize} />
        <span className='pizza-section__cost'>от {cost} руб.</span>
        <Button className='button pizza-section__btn'>
          <span className='pizza-section__btn-inner_order'>Заказать</span>
          <span className='pizza-section__btn-inner_cost'>от {cost} руб.</span>
        </Button>
      </div>
    </div>
  )
}

export default PizzaCard
