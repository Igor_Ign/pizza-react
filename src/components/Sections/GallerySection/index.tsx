import React from 'react'

import galleryItem1 from '../../../assets/img/gallery/inst01.jpg'
import galleryItem2 from '../../../assets/img/gallery/inst02.jpg'
import galleryItem3 from '../../../assets/img/gallery/inst03.jpg'
import galleryItem4 from '../../../assets/img/gallery/inst04.jpg'
import galleryItem5 from '../../../assets/img/gallery/inst05.jpg'
import galleryItem6 from '../../../assets/img/gallery/inst06.jpg'
import galleryItem7 from '../../../assets/img/gallery/inst07.jpg'
import galleryItem8 from '../../../assets/img/gallery/inst08.jpg'
import galleryItem9 from '../../../assets/img/gallery/inst09.jpg'
import galleryItem10 from '../../../assets/img/gallery/inst10.jpg'

import './gallerySectionStyles.sass'

const GallerySection = () => {
  return (
    <section className='gallery-section'>
      <h2 className='gallery-section__title'>Следи за нами в Instagram</h2>
      <h4 className='gallery-section__instagram'>@pizzamenu</h4>
      <div className='gallery-section__gallery'>
        <img src={galleryItem1} alt='galleryItem' className='gallery-section__gallery-item' />
        <img src={galleryItem2} alt='galleryItem' className='gallery-section__gallery-item' />
        <img src={galleryItem3} alt='galleryItem' className='gallery-section__gallery-item' />
        <img src={galleryItem4} alt='galleryItem' className='gallery-section__gallery-item' />
        <img src={galleryItem5} alt='galleryItem' className='gallery-section__gallery-item' />
        <img src={galleryItem6} alt='galleryItem' className='gallery-section__gallery-item' />
        <img src={galleryItem7} alt='galleryItem' className='gallery-section__gallery-item' />
        <img src={galleryItem8} alt='galleryItem' className='gallery-section__gallery-item' />
        <img src={galleryItem9} alt='galleryItem' className='gallery-section__gallery-item' />
        <img src={galleryItem10} alt='galleryItem' className='gallery-section__gallery-item' />
      </div>
    </section>
  )
}

export default GallerySection
