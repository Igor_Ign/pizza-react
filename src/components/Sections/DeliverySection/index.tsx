import React from 'react'

import DeliveryInfoCard from './DeliveryInfoCard'

import orderIcon from '../../../assets/img/svg/delivery/order.svg'
import deliveryIcon from '../../../assets/img/svg/delivery/delivery.svg'
import payIcon from '../../../assets/img/svg/delivery/pay.svg'

import './deliverySectionStyle.sass'

const DeliverySection = () => {
  return (
    <section className='delivery-section'>
      <div className='container'>
        <h2 className='delivery-section__title'>Доставка и оплата</h2>
        <div className='delivery-section__wrapper'>
          <DeliveryInfoCard
            title='Заказ'
            description='После оформления заказа мы с вами для уточнения деталей.'
            alt='order'
            icon={orderIcon}
          />
          <DeliveryInfoCard
            title='Доставка курьером'
            description='Мы доставим вашу пиццу горячей. Бесплатная доставка по городу.'
            alt='delivery'
            icon={deliveryIcon}
          />
          <DeliveryInfoCard
            title='Оплата'
            description='Оплатить можно наличными или картой курьеру. И золотом тоже можно.'
            alt='pay'
            icon={payIcon}
          />
        </div>
      </div>
    </section>
  )
}

export default DeliverySection
