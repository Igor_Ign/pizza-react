import React, { FC } from 'react'

interface DeliveryInfoCardProps {
  icon: string
  alt: string
  title: string
  description: string
}

const DeliveryInfoCard: FC<DeliveryInfoCardProps> = ({ description, title, icon, alt }) => {
  return (
    <div className='delivery-section__card'>
      <img src={icon} alt={alt} />
      <div>
        <h4 className='delivery-section__card-title'>{title}</h4>
        <p className='delivery-section__card-description'>{description}</p>
      </div>
    </div>
  )
}

export default DeliveryInfoCard
