import React from 'react'

import menuItems from './mockMenu'

import './menuStyles.sass'

const Menu = () => {
  return (
    <nav className='header__menu'>
      <ul className='header__menu-list'>
        {menuItems.map((item) => (
          <li key={item} className='header__menu-item'>
            {item}
          </li>
        ))}
      </ul>
    </nav>
  )
}

export default Menu
