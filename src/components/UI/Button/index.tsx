import React, { FC, ReactNode } from 'react'

import './buttonStyles.sass'

interface ButtonProps
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  children: ReactNode
  className?: string
}

const Button: FC<ButtonProps> = ({ children, className, ...props }) => (
  <button className={`button ${className}`} {...props}>
    <span className='button__inner'>{children}</span>
  </button>
)

export default Button
