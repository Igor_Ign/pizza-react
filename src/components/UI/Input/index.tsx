import React, { FC, useState } from 'react'

import './inputStyles.sass'

interface InputProps
  extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  className?: string
  error?: string
  label: string
  placeholder?: string
}

const Input: FC<InputProps> = ({ className, label, placeholder, error, ...props }) => {
  const [isFocused, setIsFocused] = useState(false)
  const [value, setValue] = useState('')

  const onChangeHandler = (ev: React.ChangeEvent<HTMLInputElement>) =>
    setValue(ev.target.value.trim())

  const onFocusHandler = () => setIsFocused(true)

  const onBlurHandler = () => setIsFocused(false)

  return (
    <>
      <label className='field' htmlFor={`input ${label}`}>
        <input
          id={`input ${label}`}
          className={`field__input ${className}`}
          type='text'
          value={value}
          onChange={onChangeHandler}
          {...props}
          onFocus={onFocusHandler}
          onBlur={onBlurHandler}
        />
        <span
          className={`field__input-placeholder ${isFocused || value.length > 0 ? 'focused' : ''}`}
        >
          {placeholder}
        </span>
      </label>
      {error && <small>{error}</small>}
    </>
  )
}

export default Input
