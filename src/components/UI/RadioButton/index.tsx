import React, { FC, useMemo } from 'react'

import './radioButtonStyles.sass'

interface RadioButtonProps
  extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  title: string
  className?: string
}

const RadioButton: FC<RadioButtonProps> = ({ title, className, ...props }) => {
  const cls = useMemo(() => {
    if (props.checked) {
      return 'radio-button__wrapper_checked'
    }

    if (props.disabled) {
      return 'radio-button__wrapper_disabled'
    }

    return ''
  }, [props.checked, props.disabled])

  return (
    <label htmlFor={`radio ${props.name}`} className={`radio-button ${className}`}>
      <input id={`radio ${props.name}`} type='radio' {...props} />
      <span className={`radio-button__wrapper ${cls}`} aria-disabled={true} />
      <span className='radio-button__title'>{title}</span>
    </label>
  )
}

export default RadioButton
