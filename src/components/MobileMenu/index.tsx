import React, { FC } from 'react'
import menuItems from '../Menu/mockMenu'

import closeIcon from '../../assets/img/svg/close.svg'
import logoIcon from '../../assets/img/svg/logo.svg'

import './mobileMenuStyles.sass'

interface MobileMenuProps {
  isOpen: boolean
  onChange: () => void
}

const MobileMenu: FC<MobileMenuProps> = ({ isOpen, onChange }) => {
  return (
    <nav className={`mobile-menu ${!isOpen ? 'close' : ''}`}>
      <div className='mobile-menu__wrapper'>
        <img className='mobile-menu__logo' src={logoIcon} alt='logo' />
        <button className='mobile-menu__close' onClick={onChange}>
          <img src={closeIcon} alt='close' />
        </button>
      </div>
      <ul>
        {menuItems.map((item) => (
          <li key={item} className='mobile-menu__item'>
            <a className='mobile-menu__link' href='#'>
              {item}
            </a>
          </li>
        ))}
      </ul>
      <div className='mobile-menu__footer'>
        <p className='mobile-menu__footer-title'>Заказать по телефону</p>
        <a className='mobile-menu__footer-phone' href='tel:+79184326587'>
          +7 (918) 432-65-87
        </a>
        <p className='mobile-menu__footer-time'>Ежедневно с 9:00 до 23:00</p>
        <p className='mobile-menu__footer-lang'>English</p>
      </div>
    </nav>
  )
}

export default MobileMenu
