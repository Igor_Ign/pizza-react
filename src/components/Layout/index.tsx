import React, { FC, ReactNode, useEffect } from 'react'

import Header from '../Header'
import Footer from '../Footer'
import Modal from '../Modal'
import useToggle from '../../hooks/useToggle'

interface LayoutProps {
  children: ReactNode
}

const Layout: FC<LayoutProps> = ({ children }) => {
  const [isOpenModal, setIsOpenModal] = useToggle()
  const [isOpenMenu, setIsOpenMenu] = useToggle()

  useEffect(() => {
    document.body.style.overflow = isOpenModal || isOpenMenu ? 'hidden' : 'auto'
  }, [isOpenModal, isOpenMenu])

  return (
    <>
      <Header isOpenMenu={isOpenMenu} menuToggle={setIsOpenMenu} modalToggle={setIsOpenModal} />
      <Modal isOpen={isOpenModal} modalToggle={setIsOpenModal} />
      <main>{children}</main>
      <Footer />
    </>
  )
}

export default Layout
