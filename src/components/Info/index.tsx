import React, { FC } from 'react'

import './infoStyles.sass'

interface InfoProps {
  icon: string
  alt: string
  isLink?: boolean
  href?: string
  title: string
  subtitle: string
  onClick?: () => void
  counter?: number
}

const Info: FC<InfoProps> = ({
  subtitle,
  isLink = false,
  title,
  icon,
  href,
  alt,
  onClick,
  counter,
}) => {
  return (
    <div className='header__info' onClick={onClick}>
      <div className='header__info-icon-container'>
        {counter && <div className='header__info-counter'>3</div>}
        <img className='header__info-icon' src={icon} alt={alt} />
      </div>
      <div className='header__info-block'>
        {isLink ? (
          <a href={href} className='header__info-title header__info-title_link'>
            {title}
          </a>
        ) : (
          <div className='header__info-title'>{title}</div>
        )}
        <div className='header__info-subtitle'>{subtitle}</div>
      </div>
    </div>
  )
}

export default Info
