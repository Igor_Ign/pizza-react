import { IFilter } from '../models/filter.model'
import TPizzaTypes from '../models/pizza-types.model'

const FILTERS: IFilter[] = [
  {
    id: 0,
    type: TPizzaTypes.ALL,
  },
  {
    id: 1,
    type: TPizzaTypes.PEPPER,
  },
  {
    id: 2,
    type: TPizzaTypes.MEAT,
  },
  {
    id: 3,
    type: TPizzaTypes.CHEESE,
  },
  {
    id: 4,
    type: TPizzaTypes.VEGAN,
  },
]

export default FILTERS
